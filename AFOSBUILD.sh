rm -rf /opt/ANDRAX/gophish

go build

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Go build... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip gophish

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/gophish

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
